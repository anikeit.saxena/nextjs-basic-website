import styles from '../styles/Home.module.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import Card from './card';
import Table from './table';

export default function Main(){
    return (
        <main>
        
        <div className="pricing-header p-3 pb-md-4 mx-auto text-center" style={{padding: 16}}>
            <h1 className='display-4 fw-normal'>
            Pricing
            </h1>
            <div className='fs-5 text-muted'>Quickly build an effective pricing table for your potential customers with this Bootstrap example. It’s built with default Bootstrap components and utilities with little customization.
            </div>
        </div>
        
        <Card />

        <Table /> 
        
      </main>
    )
}