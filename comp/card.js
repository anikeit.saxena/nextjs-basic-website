import styles from '../styles/Home.module.css'
import 'bootstrap/dist/css/bootstrap.min.css';

export default function Card(){
    return (
        <div className="row row-cols-1 row-cols-md-3 mb-3 text-center">
            <div className='col'>
                <div className="card mb-4 rounded-3 shadow-sm"> 
                    <div className={styles.cardheader}>
                        <div style={{alignSelf: 'center'}}>Free</div>
                    </div>
                    <div className="card-body">
                        <h1 className='card-title pricing-card-title'>$0
                        <small className='text-muted fw-light'> /mo </small>
                        </h1>
                    </div>
                    <div>
                        <ul className='list-unstyled mb-4'>
                            <li>10 users included</li>
                            <li>2 GB of storage</li>
                            <li>Email support</li>
                            <li>Help center access</li>
                        </ul>
                    </div>
                    <div className={styles.btn}>
                    <button type='button' className='w-100 btn btn-lg btn-outline-primary'>
                        Sign up for free
                    </button>
                    </div>
                </div>
            </div>
            <div className='col'>
                <div className="card mb-4 rounded-3 shadow-sm"> 
                    <div className={styles.cardheader}>
                        <div style={{alignSelf: 'center'}}>Pro</div>
                    </div>
                    <div className='card-body'>
                        <h1 className='card-title pricing-card-title'>$15
                        <small className='text-muted fw-light'> /mo </small>
                        </h1>
                    </div>
                    <div>
                        <ul className='list-unstyled mb-4'>
                            <li>20 users included</li>
                            <li>10 GB of storage</li>
                            <li>Priority Email support</li>
                            <li>Help center access</li>
                        </ul>
                    </div>
                    <div className={styles.btn}>
                    <button type='button' className='w-100 btn btn-lg btn-primary'>
                        Get Started
                    </button>
                    </div>
                </div>
            </div>
            <div className='col'>
                <div className="card mb-4 rounded-3 shadow-sm" style={{borderColor: '#0070f3'}}> 
                    <div className={styles.cardheader} style={{backgroundColor: '#0070f3'}}>
                        <div style={{alignSelf: 'center', color:"whitesmoke"}}>Enterprise</div>
                    </div>
                    <div className="card-body">
                        <h1 className='card-title pricing-card-title'>$29
                        <small className='text-muted fw-light'> /mo </small>
                        </h1>
                    </div>
                    <div>
                        <ul className='list-unstyled mb-4'>
                            <li>30 users included</li>
                            <li>15 GB of storage</li>
                            <li>Phone and Email support</li>
                            <li>Help center access</li>
                        </ul>
                    </div>
                    <div className={styles.btn}>
                    <button type='button' className='w-100 btn btn-lg btn-primary'>
                        Sign up for free
                    </button>
                    </div>
                </div>  
            </div>    
        </div>
    )
}