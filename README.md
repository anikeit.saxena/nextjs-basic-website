# NextJS Basic Website
In this mini-project, I have created a simple static website which is the clone of Bootstrap's Pricing page using NextJS and React-Bootstrap.

The website is reponsive and suitable for all types of devices. It also has various links and some styling when we hover on some of the elements.